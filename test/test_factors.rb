require 'minitest/autorun'
require 'fileutils'
require 'json'
require_relative '../lib/factors'

class TestFactors < MiniTest::Unit::TestCase

  def setup
    @factors = Factors.new
  end

  def teardown
    if Dir.exist?(@factors.tmp)
      FileUtils.rm_rf(@factors.tmp)
    end
    @factors = nil
  end

  def test_factors_of_1
    actual = @factors.factors(1)
    assert_equal 0, actual.size
  end

  def test_factors_of_2
    actual = @factors.factors(2)
    assert_equal 0, actual.size
  end

  def test_factors_of_3
    actual = @factors.factors(3)
    assert_equal 0, actual.size
  end

  def test_factors_of_4
    actual = @factors.factors(4)
    assert_equal 1, actual.size
    assert_equal 2, actual[0]
  end

  def test_factors_of_5
    actual = @factors.factors(5)
    assert_equal 0, actual.size
  end

  def test_factors_of_6
    actual = @factors.factors(6)
    assert_equal 2, actual.size
    assert_equal 3, actual[0]
    assert_equal 2, actual[1]
  end

  def test_factors_of_7
    actual = @factors.factors(7)
    assert_equal 0, actual.size
  end

  def test_factors_of_8
    actual = @factors.factors(8)
    assert_equal 2, actual.size
    assert_equal 4, actual[0]
    assert_equal 2, actual[1]
  end
  
  def test_factors_of_9
    actual = @factors.factors(9)
    assert_equal 1, actual.size
    assert_equal 3, actual[0]
  end

  def test_factors_of_10
    actual = @factors.factors(10)
    assert_equal 2, actual.size
    assert_equal 5, actual[0]
    assert_equal 2, actual[1]
  end

  def test_challenge_example
    input = [10, 5, 2, 20]
    actual = @factors.challenge(input)
    input.each {|i| assert actual.has_key?(i), "Key #{i}" }
    [5,2].each {|i| assert_includes actual[10], i}
    assert_empty actual[5]
    assert_empty actual[2]
    [10,5,2].each {|i| assert_includes actual[20], i}
  end

  def test_challenge_reverse_example
    input = [10, 5, 2, 20]
    actual = @factors.challenge_reverse(input)
    input.each {|i| assert actual.has_key?(i), "Key #{i}" }
    assert_includes actual[10], 20
    [10,20].each {|i| assert_includes actual[5], i}
    [10,20].each {|i| assert_includes actual[2], i}
    assert_empty actual[20]
  end

  def test_cached_challenge_example
    input = [10, 5, 2, 20]
    # verify the results are cached
    actual = @factors.cached_challenge(input)
    assert Dir.exist?(@factors.tmp)
    assert File.exist?(File.join(@factors.tmp, @factors.fnv1a(input).to_s))
    # verify the cached results
    actual = @factors.cached_challenge(input)
    input.each {|i| assert actual.has_key?(i), "Key #{i}" }
    [5,2].each {|i| assert_includes actual[10], i}
    assert_empty actual[5]
    assert_empty actual[2]
    [10,5,2].each {|i| assert_includes actual[20], i}
  end

end

