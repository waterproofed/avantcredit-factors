
require 'prime'
require 'json'
require 'tmpdir'

class Factors

  attr_reader(:tmp)

  def initialize
    @tmp = Dir.mktmpdir('factors')
  end

  # calculate the factors of the input number, returning a sorted
  # array containing both the prime and non-prime factors of that
  # number in descending order
  def factors(num)
    return num.prime_division.map{|p, e| (1..e).map {|x| p**x}}.flatten.uniq.select{|i| i < num}.sort.reverse
  end

  # for each number in the array select other members of the array that are:
  # 1) factors of itself, and
  # 2) factors of first element of the array or the first element itself
  #
  # the results are returned as a hash whose keys are the members of
  # the input array and whose values satisfy the above conditions
  def challenge(arr)
    if arr.nil? or arr.empty?
      return Hash.new
    end
    a = arr[0]
    f = factors(a).push(a)
    k = arr.select{|i| f.include?(i)}
    return Hash[arr.map{|i| [i, k.select{|j| i != j and i % j == 0}.sort.reverse]}]
  end

  def challenge_reverse(arr)
    h = challenge(arr)
    return Hash[arr.map{|i| [i, h.select{|k,v| v.include?(i)}.keys.sort.reverse]}]
  end

  # fowler-noll-vo 1a hash over an array of integers
  # returns a 32 bit integer
  def fnv1a(arr)
    max = (2**32)+1;
    val = 2166136261
    arr.map{|i| [i].pack("N").bytes}.flatten.each do |b|
      val ^= b
      val *= 16777619
      val %= max
    end
    return val
  end

  def cached_challenge(arr)
    filename = fnv1a(arr)
    filepath = File.join(@tmp, filename.to_s)
    if File.exist?(filepath)
      result = JSON.parse(IO.read(filepath))
      result = Hash[result.map{|k,v| [k.to_i, v]}]
    else
      result = challenge(arr)
      IO.write(filepath, result.to_json)
    end
    return result
  end

  def cached_challenge_reverse(arr)
    h = cached_challenge(arr)
    return Hash[arr.map{|i| [i, h.select{|k,v| v.include?(i)}.keys.sort.reverse]}]
  end

end

