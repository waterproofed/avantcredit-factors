
# factors and caching

this is the solution to the factors and caching programming challenge

## installation

```bash

    git clone git@bitbucket.org:waterproofed/avantcredit-factors.git

    cd avantcredit-factors

    bundle install

```

## tests

```bash

    rake

```

## notes

this was developed under the default ruby installation of OSX

    ruby 2.0.0p481 (2014-05-08 revision 45883) [universal.x86_64-darwin14]

## remarks

the problem is poorly described and under-defined.  it would help to have additional
examples which can be used as test cases.

